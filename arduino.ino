/******************************************************************* 
*  Arduino VGA is a game consolle for VGA monitors,           * 
*  written by Roberto Melzi, running for Arduino software          * 
*  IDE version 1.6.4                                               * 
*  VGAx library does not work for newer or elder software          *
*  versions.                                                       * 
*  VGAx Library written By Sandro Maffiodo aka Smaffer.            * 
*  https://github.com/smaffer/vgax                                 *            
*                                                                  * 
*******************************************************************/ 

#include <VGAX.h>
#include <math.h>
#include <VGAXUtils.h> 

#define FNT_NANOFONT_HEIGHT 6
#define FNT_NANOFONT_SYMBOLS_COUNT 95
#define PADDLE_HEIGHT 8
#define PADDLE_WIDTH 2
#define RIGHT_PADDLE_X (VGAX_WIDTH-4)
#define LEFT_PADDLE_X 2
#define MAX_Y_VELOCITY 0.1
#define WHEEL_ONE_PIN 2 //analog
#define WHEEL_TWO_PIN 1 //analog
#define BUTTON_1_PIN 13 //digital 
#define BUTTON_2_PIN 5  //digital
#define BUTTON_3_PIN 11 //digital
#define BUTTON_4_PIN 12 //digital
#define BUTTON_5_PIN 10 //digital
// NB: pin A0 is used for the sound 

VGAX vga;
VGAXUtils vgaU;

//data size=570 bytes
const unsigned char fnt_nanofont_data[FNT_NANOFONT_SYMBOLS_COUNT][1+FNT_NANOFONT_HEIGHT] PROGMEM={
{ 1, 128, 128, 128, 0, 128, 0, }, //glyph '!' code=0
{ 3, 160, 160, 0, 0, 0, 0, }, //glyph '"' code=1
//{ 5, 80, 248, 80, 248, 80, 0, },  //glyph '#' code=2
{ 5, 240, 240, 240, 240, 240, 240, },  //glyph '#' = 'full rectangle' code=2
//{ 5, 32, 160, 240, 120, 32, 32, },  //glyph '#' = 'planeR' code=2
{ 3, 80, 32, 112, 112, 32, 0, },  //glyph '$' = 'bomb' code=3
{ 5, 32, 40, 120, 240, 32, 32, },  //glyph '%' = 'planeL' code=4
{ 5, 96, 144, 104, 144, 104, 0, },  //glyph '&' code=5
{ 5, 248, 248, 248, 248, 248, 248, },  //glyph ''' = 'rectangle 5 x 6' code=6
//{ 5, 120, 160, 112, 40, 240, 0, },  //glyph '$' code=3
//{ 5, 136, 16, 32, 64, 136, 0, },  //glyph '%' code=4
//{ 5, 96, 144, 104, 144, 104, 0, },  //glyph '&' code=5
//{ 2, 128, 64, 0, 0, 0, 0, },  //glyph ''' code=6
{ 2, 64, 128, 128, 128, 64, 0, }, //glyph '(' code=7
{ 2, 128, 64, 64, 64, 128, 0, },  //glyph ')' code=8
{ 3, 0, 160, 64, 160, 0, 0, },  //glyph '*' code=9
{ 3, 0, 64, 224, 64, 0, 0, }, //glyph '+' code=10
{ 2, 0, 0, 0, 0, 128, 64, },  //glyph ',' code=11
{ 3, 0, 0, 224, 0, 0, 0, }, //glyph '-' code=12
{ 1, 0, 0, 0, 0, 128, 0, }, //glyph '.' code=13
{ 5, 8, 16, 32, 64, 128, 0, },  //glyph '/' code=14
{ 4, 96, 144, 144, 144, 96, 0, }, //glyph '0' code=15
{ 3, 64, 192, 64, 64, 224, 0, },  //glyph '1' code=16
{ 4, 224, 16, 96, 128, 240, 0, }, //glyph '2' code=17
{ 4, 224, 16, 96, 16, 224, 0, },  //glyph '3' code=18
{ 4, 144, 144, 240, 16, 16, 0, }, //glyph '4' code=19
{ 4, 240, 128, 224, 16, 224, 0, },  //glyph '5' code=20
{ 4, 96, 128, 224, 144, 96, 0, }, //glyph '6' code=21
{ 4, 240, 16, 32, 64, 64, 0, }, //glyph '7' code=22
{ 4, 96, 144, 96, 144, 96, 0, },  //glyph '8' code=23
{ 4, 96, 144, 112, 16, 96, 0, },  //glyph '9' code=24
{ 1, 0, 128, 0, 128, 0, 0, }, //glyph ':' code=25
{ 2, 0, 128, 0, 0, 128, 64, },  //glyph ';' code=26
{ 3, 32, 64, 128, 64, 32, 0, }, //glyph '<' code=27
{ 3, 0, 224, 0, 224, 0, 0, }, //glyph '=' code=28
{ 3, 128, 64, 32, 64, 128, 0, },  //glyph '>' code=29
{ 4, 224, 16, 96, 0, 64, 0, },  //glyph '?' code=30
{ 4, 96, 144, 176, 128, 112, 0, },  //glyph '@' code=31
{ 4, 96, 144, 240, 144, 144, 0, },  //glyph 'A' code=32
{ 4, 224, 144, 224, 144, 224, 0, }, //glyph 'B' code=33
{ 4, 112, 128, 128, 128, 112, 0, }, //glyph 'C' code=34
{ 4, 224, 144, 144, 144, 224, 0, }, //glyph 'D' code=35
{ 4, 240, 128, 224, 128, 240, 0, }, //glyph 'E' code=36
{ 4, 240, 128, 224, 128, 128, 0, }, //glyph 'F' code=37
{ 4, 112, 128, 176, 144, 112, 0, }, //glyph 'G' code=38
{ 4, 144, 144, 240, 144, 144, 0, }, //glyph 'H' code=39
{ 3, 224, 64, 64, 64, 224, 0, },  //glyph 'I' code=40
{ 4, 240, 16, 16, 144, 96, 0, },  //glyph 'J' code=41
{ 4, 144, 160, 192, 160, 144, 0, }, //glyph 'K' code=42
{ 4, 128, 128, 128, 128, 240, 0, }, //glyph 'L' code=43
{ 5, 136, 216, 168, 136, 136, 0, }, //glyph 'M' code=44
{ 4, 144, 208, 176, 144, 144, 0, }, //glyph 'N' code=45
{ 4, 96, 144, 144, 144, 96, 0, }, //glyph 'O' code=46
{ 4, 224, 144, 224, 128, 128, 0, }, //glyph 'P' code=47
{ 4, 96, 144, 144, 144, 96, 16, },  //glyph 'Q' code=48
{ 4, 224, 144, 224, 160, 144, 0, }, //glyph 'R' code=49
{ 4, 112, 128, 96, 16, 224, 0, }, //glyph 'S' code=50
{ 3, 224, 64, 64, 64, 64, 0, }, //glyph 'T' code=51
{ 4, 144, 144, 144, 144, 96, 0, },  //glyph 'U' code=52
{ 3, 160, 160, 160, 160, 64, 0, },  //glyph 'V' code=53
{ 5, 136, 168, 168, 168, 80, 0, },  //glyph 'W' code=54
{ 4, 144, 144, 96, 144, 144, 0, },  //glyph 'X' code=55
{ 3, 160, 160, 64, 64, 64, 0, },  //glyph 'Y' code=56
{ 4, 240, 16, 96, 128, 240, 0, }, //glyph 'Z' code=57
{ 2, 192, 128, 128, 128, 192, 0, }, //glyph '[' code=58
{ 5, 128, 64, 32, 16, 8, 0, },  //glyph '\' code=59
{ 2, 192, 64, 64, 64, 192, 0, },  //glyph ']' code=60
{ 5, 32, 80, 136, 0, 0, 0, }, //glyph '^' code=61
{ 4, 0, 0, 0, 0, 240, 0, }, //glyph '_' code=62
{ 2, 128, 64, 0, 0, 0, 0, },  //glyph '`' code=63
{ 3, 0, 224, 32, 224, 224, 0, },  //glyph 'a' code=64
{ 3, 128, 224, 160, 160, 224, 0, }, //glyph 'b' code=65
{ 3, 0, 224, 128, 128, 224, 0, }, //glyph 'c' code=66
{ 3, 32, 224, 160, 160, 224, 0, },  //glyph 'd' code=67
{ 3, 0, 224, 224, 128, 224, 0, }, //glyph 'e' code=68
{ 2, 64, 128, 192, 128, 128, 0, },  //glyph 'f' code=69
{ 3, 0, 224, 160, 224, 32, 224, },  //glyph 'g' code=70
{ 3, 128, 224, 160, 160, 160, 0, }, //glyph 'h' code=71
{ 1, 128, 0, 128, 128, 128, 0, }, //glyph 'i' code=72
{ 2, 0, 192, 64, 64, 64, 128, },  //glyph 'j' code=73
{ 3, 128, 160, 192, 160, 160, 0, }, //glyph 'k' code=74
{ 1, 128, 128, 128, 128, 128, 0, }, //glyph 'l' code=75
{ 5, 0, 248, 168, 168, 168, 0, }, //glyph 'm' code=76
{ 3, 0, 224, 160, 160, 160, 0, }, //glyph 'n' code=77
{ 3, 0, 224, 160, 160, 224, 0, }, //glyph 'o' code=78
{ 3, 0, 224, 160, 160, 224, 128, }, //glyph 'p' code=79
{ 3, 0, 224, 160, 160, 224, 32, },  //glyph 'q' code=80
{ 3, 0, 224, 128, 128, 128, 0, }, //glyph 'r' code=81
{ 2, 0, 192, 128, 64, 192, 0, },  //glyph 's' code=82
{ 3, 64, 224, 64, 64, 64, 0, }, //glyph 't' code=83
{ 3, 0, 160, 160, 160, 224, 0, }, //glyph 'u' code=84
{ 3, 0, 160, 160, 160, 64, 0, },  //glyph 'v' code=85
{ 5, 0, 168, 168, 168, 80, 0, },  //glyph 'w' code=86
{ 3, 0, 160, 64, 160, 160, 0, },  //glyph 'x' code=87
{ 3, 0, 160, 160, 224, 32, 224, },  //glyph 'y' code=88
{ 2, 0, 192, 64, 128, 192, 0, },  //glyph 'z' code=89
{ 3, 96, 64, 192, 64, 96, 0, }, //glyph '{' code=90
{ 1, 128, 128, 128, 128, 128, 0, }, //glyph '|' code=91
{ 3, 192, 64, 96, 64, 192, 0, },  //glyph '}' code=92
//{ 3, 96, 192, 0, 0, 0, 0, },  //glyph '~' code=93
{ 5, 32, 160, 240, 120, 32, 32, }, //= 'planeR' code=93
{ 4, 48, 64, 224, 64, 240, 0, },  //glyph '£' code=94
};

static const char str10[] PROGMEM="#"; 
static const char str15[] PROGMEM="Rojo ganador"; 
static const char str16[] PROGMEM="Verde ganador"; 
static const char str21[] PROGMEM="########";
static const char str30[] PROGMEM="Menu Pong"; 
static const char str31[] PROGMEM="Jugar"; 
static const char str32[] PROGMEM="Salir."; 
static const char str40[] PROGMEM="o"; 

void setup() {
  //Serial.begin(9600); 
  vga.begin();
  randomSeed(1);  
}
//-------------------------------- variables de definicion Pong ----------------------------
boolean buttonOneStatus; 
boolean buttonTwoStatus; 
boolean buttonThreeStatus; 
boolean buttonFourStatus; 
boolean buttonFiveStatus; 
byte wheelOnePosition; //potenciometro 1
byte wheelTwoPosition; //potenciometro 2
byte state = 1; 
byte shot; // pong
byte rightPaddleY;
byte leftPaddleY;
byte rightPaddleY0; 
byte leftPaddleY0; 
byte ballX; 
byte ballY; 
byte scoreR; 
byte scoreL; 
int beginning
byte ticPosition = 12; 
float ballPX;
int ballPY; 
float ballVx;
float ballVy;
int speedT; 
byte hitScore; 
byte lives = 8; //---------- vidas del pong : lives = 8 ----
byte iDel;
byte jDel; 
byte ballXhit; 
byte color; 
static const float speedIncrement PROGMEM = 1.259921;

//----------------------------------------------------Variables definition end --------------------------------

void processInputs() {
  buttonOneStatus = digitalRead(BUTTON_1_PIN); // -------------
  buttonTwoStatus = digitalRead(BUTTON_2_PIN); // ----------------
  buttonThreeStatus = digitalRead(BUTTON_3_PIN); // -------------
  buttonFourStatus = digitalRead(BUTTON_4_PIN); // --------------
  buttonFiveStatus = digitalRead(BUTTON_5_PIN); // --------------
  wheelOnePosition = 127 - byte(analogRead(WHEEL_ONE_PIN)/8); //para cambiar la dirección de la rueda quitar "127 -" ---------------------
  wheelTwoPosition = byte(analogRead(WHEEL_TWO_PIN)/8); 
}

void buttonZeroing(){
  buttonOneStatus = 0;
  buttonTwoStatus = 0; 
  buttonThreeStatus = 0; 
  buttonFourStatus = 0; 
}

void parameterPongIni() { // ------------ Pong ---------------------------------------------------------------- 
   parameterPongIni0(); 
   lives = 8;  
}

void parameterPongIni0() { // ------------ Pong ---------------------------------------------------------------- 
  rightPaddleY0 = (rightPaddleY + 26)%50 + 1; 
  leftPaddleY0 = (leftPaddleY + 26)%50 + 1; 
  ballVx = random(100, 160)/1000. + (scoreL + scoreR)/200.; //----- pong horizontal speed ----- from 0.1 to 0.16, up to 0.23 -------- 
  ballVy = random(-40, 40)/1000.;                           //----- pong vertical speed --------------- 
}

void waitForStart(){ //-------------------------------------- Pong waiting screen --------------------------------------------------
   buttonZeroing(); 
   if(scoreR == lives || scoreR == 0){
      processInputs(); 
      ballY = byte(((wheelTwoPosition) * (VGAX_HEIGHT-PADDLE_HEIGHT-1))/ 128 + 4);
      drawGameScreen(); 
   }
   if(scoreL == lives){
      processInputs(); 
      ballY = byte(((wheelOnePosition) * (VGAX_HEIGHT-PADDLE_HEIGHT-1))/ 128 + 4);
      drawGameScreen(); 
    }
    if (buttonOneStatus == 1 || buttonTwoStatus == 1){ 
       ballPX = ballX; 
       ballPY = ballY*100;
       parameterPongIni(); 
       scoreL = 0; 
       scoreR = 0; 
       drawScore(); 
       buttonZeroing();
       state = 3; 
    }
    if (buttonThreeStatus == 1) { state = 0; vga.delay(200); }
}

void drawPaddles(){ // Pong
    // ----------------- draw right paddle -----------------------------------------------
    rightPaddleY = byte(((wheelOnePosition) * (VGAX_HEIGHT-PADDLE_HEIGHT-1))/ 128 + 1);
    shot = RIGHT_PADDLE_X; 
    if (rightPaddleY != rightPaddleY0 || beginning > 0){
      vgaU.draw_column(shot, rightPaddleY0, rightPaddleY0 + PADDLE_HEIGHT, 0);
      vgaU.draw_column(shot + 1, rightPaddleY0, rightPaddleY0 + PADDLE_HEIGHT, 0);
      vgaU.draw_column(shot, rightPaddleY, rightPaddleY + PADDLE_HEIGHT, 2);
      vgaU.draw_column(shot + 1, rightPaddleY, rightPaddleY + PADDLE_HEIGHT, 2);
    }
    // ----------------- draw left paddle -------------------------------------------------
    leftPaddleY = byte(((wheelTwoPosition) * (VGAX_HEIGHT-PADDLE_HEIGHT-1))/ 128 + 1);
    shot = LEFT_PADDLE_X;
    if (leftPaddleY != leftPaddleY0 || beginning > 0){
       vgaU.draw_column(shot, leftPaddleY0, leftPaddleY0 + PADDLE_HEIGHT, 0);
       vgaU.draw_column(shot + 1, leftPaddleY0, leftPaddleY0 + PADDLE_HEIGHT, 0);
       vgaU.draw_column(shot, leftPaddleY, leftPaddleY + PADDLE_HEIGHT, 1);
       vgaU.draw_column(shot + 1, leftPaddleY, leftPaddleY + PADDLE_HEIGHT, 1);
    }
}

void drawBall(){ // Pong
   if ((ballX != iDel) || (ballY != jDel)){
      vga.putpixel(iDel, jDel, 0);
      vga.putpixel(iDel, jDel + 1, 0);
      vga.putpixel(iDel + 1, jDel, 0);
      vga.putpixel(iDel + 1, jDel + 1, 0);
      vga.putpixel(ballX, ballY, 3);
      vga.putpixel(ballX, ballY + 1, 3);
      vga.putpixel(ballX + 1, ballY, 3);
      vga.putpixel(ballX + 1, ballY + 1, 3);
   }
}

void drawNet(){ // Pong 
   for(int i=1; i<VGAX_HEIGHT - 4; i += 6) {
      vgaU.draw_column(VGAX_WIDTH/2, i, i + 3, 3);
   }
}

void drawBorder() { // Trazado del borde del recuadro Pong
    vgaU.draw_line(0,0,VGAX_WIDTH-1,0,3);
    vgaU.draw_line(0,VGAX_HEIGHT-1,VGAX_WIDTH-1,VGAX_HEIGHT-1,3);
    vgaU.draw_line(0,0,0,VGAX_HEIGHT-1,3);
    vgaU.draw_line(VGAX_WIDTH-1,0,VGAX_WIDTH-1,VGAX_HEIGHT,3);
}

void drawScore() { // ---------------- Pong score y ganador --------------------------------------------
    vgaPrint(str10, 52, 4, 0);
    vgaPrint(str10, 64, 4, 0);
    vgaPrintNumber(scoreL, 52, 4, 1); 
    vgaPrintNumber(scoreR, 64, 4, 2);
    if(scoreL == lives) {
       vgaPrint(str15, 12, 24, 1);
       vga.delay(3000);
       ballX = VGAX_WIDTH - 6; 
       buttonZeroing(); 
       while(buttonOneStatus == 0 && buttonTwoStatus == 0 && buttonThreeStatus == 0){
          processInputs(); 
          ballY = byte(((wheelOnePosition) * (VGAX_HEIGHT-PADDLE_HEIGHT-1))/ 128 + 4);
          drawGameScreen(); 
       }
    }
    if(scoreR == lives) {
       vgaPrint(str16, 66, 24, 2);
       vga.delay(3000);
       ballX = 4; 
       buttonZeroing(); 
       while(buttonOneStatus == 0 && buttonTwoStatus == 0 && buttonThreeStatus == 0){
          processInputs(); 
          ballY = byte(((wheelTwoPosition) * (VGAX_HEIGHT-PADDLE_HEIGHT-1))/ 128 + 4);
          drawGameScreen(); 
       }
    }
    if(scoreL == lives || scoreR == lives){ //---------------------------- Pong end --------------------- 
       buttonZeroing(); 
       do{ 
          processInputs(); 
       } while (buttonOneStatus == 0 && buttonTwoStatus == 0 && buttonThreeStatus == 0);  
       if (buttonThreeStatus == 1){ 
          state = 0;
          vga.delay(300);
       }
       else{
          vga.delay(200);
          parameterPongIni();
          drawPongStartScreen(); 
          state = 2; 
       }
    }
}

void drawPongStartScreen() { // Pong 
   vgaPrint(str10, 52, 4, 0);
   vgaPrint(str10, 64, 4, 0);
   vgaPrintNumber(0, 52, 4, 1);
   vgaPrintNumber(0, 64, 4, 2);
   vgaPrint(str16, 66, 24, 0);
   vgaPrint(str15, 12, 24, 0);
   vgaPrint(str21, 11, 24, 0);
   vgaPrint(str21, 65, 24, 0);
   drawBorder(); 
   drawGameScreen(); 
   buttonZeroing(); 
   vga.delay(500);
} 

void drawGameScreen() { // Pong 
    drawBall(); 
    drawPaddles(); 
    drawNet(); 
    iDel = ballX;
    jDel = ballY; 
    leftPaddleY0 = leftPaddleY;
    rightPaddleY0 = rightPaddleY; 
}
void vgaTone(int freq, byte time) {
   vga.tone(freq);
   vga.delay(time); 
   vga.noTone(); 
}
void vgaPrint(const char* str, byte x, byte y, byte color){
   vga.printPROGMEM((byte*)fnt_nanofont_data, FNT_NANOFONT_SYMBOLS_COUNT, FNT_NANOFONT_HEIGHT, 3, 1, str, x, y, color);
}
void vgaPrintNumber(byte number, byte x, byte y, byte color){
   char scoreChar[2];
   sprintf(scoreChar,"%d",number);
   vga.printSRAM((byte*)fnt_nanofont_data, FNT_NANOFONT_SYMBOLS_COUNT, FNT_NANOFONT_HEIGHT, 1, 1, scoreChar, x, y, color);
}

// -------------------------------------- este es el principio del Loop del juego ------------------------------------------------------------------------------------------
void loop() {
  if (state != 5) {processInputs(); } 
  if(state == 1 || state == 0) { drawStartMenu(); } 
  if(state == 2) { waitForStart(); }  // Pong 
  if(state == 3) { pong(); }
} 
// ------------------------------------------- aqui termina el Loop -----------------------------------------------------------------------------------------------------------------
 
//------------------------------------------------------- comienza la primer imagen de pantalla -------------------------------------------------------
void drawStartMenu(){
   if (state == 0) {
      state = 1; 
      ballX = 4;
      ballY = 20;
      iDel = 21;
      jDel = 21; 
      vga.clear(0);
   }
   vgaPrint(str30, 20, 2, 3);  // Menu Pong
   vgaPrint(str31, 38, 12, 2); // Jugar
   vgaPrint(str32, 38, 20, 2);  //  SALIR 
   vgaPrint(str40, 26, ticPosition, 1); 
   vga.delay(50);
   processInputs(); 
   if (buttonOneStatus == 1){ //game selector (X) moves down ---------------
      buttonZeroing(); 
      vgaPrint(str40, 26, ticPosition, 0); 
      ticPosition += 8; 
      if (ticPosition > 44) {ticPosition = 12;}
      vgaPrint(str40, 26, ticPosition, 1);
      vga.delay(200);
   }
   if (buttonThreeStatus == 0){ //game selector (X) moves up ---------------
      buttonZeroing(); 
      vgaPrint(str40, 26, ticPosition, 0); 
      ticPosition -= 8; 
      if (ticPosition < 12) {ticPosition = 12;}
      vgaPrint(str40, 26, ticPosition, 1);
      vga.delay(200);
   }
   if (buttonFourStatus == 0){ //game choice ---------------------------
      buttonZeroing(); 
       vga.delay(6000);
      vga.clear(0);
      if (ticPosition == 12) { // Pong
         lives = 8; // ----- vida de la partida 8
         scoreR = 0;
         scoreL = 0; 
         parameterPongIni0(); 
         iDel = 21;
         jDel = 21; 
         state = 2; 
         ballX = 4; 
         ballY = byte(((wheelTwoPosition) * (VGAX_HEIGHT-PADDLE_HEIGHT-1))/ 128 + 4);
         drawPongStartScreen(); 
      } 
      if (ticPosition == 20) { // SALIR
         state = 4;
         buttonZeroing(); 
         parameterPongIni0();

         vga.delay(200); 
      } 

   }
}
//----------------------------------------------- start screen end -----------------------

//----------------------------------------------- pong -------------------------------------------------- 
void pong(){ 
    
     ballPX += ballVx;     // float
     ballPY += int(ballVy*100); // int 
     ballX = byte(ballPX);
     ballY = byte(ballPY/100.);
    
     if(ballY < 1 || ballY >= VGAX_HEIGHT - 2 ) {
        ballVy = -ballVy;
        ballPY += int(ballVy*100);
        ballY = byte(ballPY/100);
        vgaTone(880,30); 
     }; 
     
     // ---------------------------------------- golpe de paleta derecha  -------------------------------------------------
     if(ballVx < 0 && ballX == LEFT_PADDLE_X+PADDLE_WIDTH -1 && ballY >= leftPaddleY - 1 && ballY <= leftPaddleY + PADDLE_HEIGHT) { 
        ballVx = -ballVx;
        ballVy += 0.2 * ((ballY - leftPaddleY + 1) - (PADDLE_HEIGHT / 2)) / (PADDLE_HEIGHT / 2);
        vgaTone(660, 30);
        drawScore();
        beginning = 2; 
        drawPaddles(); 
      }
     //----------------------------------------- golpe de paleta izquierda --------------------------------------------------
     if(ballVx > 0 && ballX == RIGHT_PADDLE_X - 1 && ballY >= rightPaddleY - 1 && ballY <= rightPaddleY + PADDLE_HEIGHT) { 
        ballVx = -ballVx;
        ballVy += 0.2 * ((ballY - rightPaddleY + 1) - (PADDLE_HEIGHT / 2)) / (PADDLE_HEIGHT / 2);
        vgaTone(660, 30);
        drawScore();
        beginning = 2; 
        drawPaddles(); 
     }
     
     //limit vertical speed
     if(ballVy > MAX_Y_VELOCITY) ballVy = MAX_Y_VELOCITY;
     if(ballVy < -MAX_Y_VELOCITY) ballVy = -MAX_Y_VELOCITY;
     
     if(ballX <= 0) { // ------------------------- pelota fuera por la derecha.. ball out from left ------------------------------------------
        vgaTone(220, 200);
        scoreR++; 
        drawScore();  
        ballX = 4;   
        beginning = 1;  
        while(buttonTwoStatus == 0 && buttonThreeStatus == 0){
           processInputs(); 
           ballY = byte(((wheelTwoPosition) * (VGAX_HEIGHT-PADDLE_HEIGHT-1))/ 128 + 4);
           drawGameScreen(); 
           beginning = 0; 
        }
        ballPX = ballX; 
        ballPY = ballY*100;
        parameterPongIni(); 
        drawBorder();
     }
     if(ballX >= VGAX_WIDTH - 2) { // ------------ pelota fuera por la izquierda... ball out from right ------------------------------------------
        vgaTone(220, 200); 
        scoreL++; 
        drawScore();      
        ballX = VGAX_WIDTH - 6;   
        beginning = 1; 
        while(buttonOneStatus == 0 && buttonThreeStatus == 0){
           processInputs(); 
           ballY = byte(((wheelOnePosition) * (VGAX_HEIGHT-PADDLE_HEIGHT-1))/ 128 + 4);
           drawGameScreen(); 
           beginning = 0; 
        }
        if (buttonThreeStatus == 0) {state == 0;} //--------------------- to exit pong ----------------------------
        ballPX = ballX; 
        ballPY = ballY*100;
        parameterPongIni(); 
        drawBorder(); 
        ballVx = -ballVx;
     }
     if (state == 1 || state == 0) {
        vga.clear(0); 
        vga.delay(300);
     }
     else {drawGameScreen();} //-------------------------------- Pong end check ---------------------
     beginning -= 1; 
     //beginning = 0; 
     beginning = constrain(beginning, 0, 10); 
  }
// ---------------------------------------- void pong() end ---------------------------------------------


// -------------------------- expansion for 4:3 monitors ------------------------------ 
void blockExtension() {
   for (int i = 0; i < 4; i++){
      blockExt[0][0] = block[0][0]*3;
      blockExt[0][1] = block[0][1]*2;
      blockExt[1][0] = block[1][0]*3;
      blockExt[1][1] = block[1][1]*2;
      blockExt[2][0] = block[2][0]*3;
      blockExt[2][1] = block[2][1]*2;
      blockExt[3][0] = block[3][0]*3;
      blockExt[3][1] = block[3][1]*2;
   }
}
 
void blockRotation(int ballXhit){
  for (int i = 0; i < 4; i++){
     blockOld[0][0] = block[0][0];
     blockOld[0][1] = block[0][1];
     blockOld[1][0] = block[1][0];
     blockOld[1][1] = block[1][1];
     blockOld[2][0] = block[2][0];
     blockOld[2][1] = block[2][1];
     blockOld[3][0] = block[3][0];
     blockOld[3][1] = block[3][1];
  }
  for (int i = 0; i < 4; i++){
     block[0][0] = blockOld[0][1]*ballXhit;
     block[0][1] = -blockOld[0][0]*ballXhit;
     block[1][0] = blockOld[1][1]*ballXhit;
     block[1][1] = -blockOld[1][0]*ballXhit;
     block[2][0] = blockOld[2][1]*ballXhit;
     block[2][1] = -blockOld[2][0]*ballXhit;
     block[3][0] = blockOld[3][1]*ballXhit;
     block[3][1] = -blockOld[3][0]*ballXhit;
  }
}
void blockTranslation(int x, int y) {
   for (int i = 0; i < 4; i++){
      blockTr[0][0] = blockExt[0][0] + x;
      blockTr[0][1] = blockExt[0][1] + y;
      blockTr[1][0] = blockExt[1][0] + x;
      blockTr[1][1] = blockExt[1][1] + y;
      blockTr[2][0] = blockExt[2][0] + x;
      blockTr[2][1] = blockExt[2][1] + y;
      blockTr[3][0] = blockExt[3][0] + x;
      blockTr[3][1] = blockExt[3][1] + y;
   }
}

void delBlock(){
  if (hitScore == 1) {hitScore = 0;} 
  else {
      for (int i = 0; i < 4; i++){
         vgaU.draw_line(blockTr[i][0],blockTr[i][1],blockTr[i][0] + 3,blockTr[i][1],0);
         vgaU.draw_line(blockTr[i][0],blockTr[i][1] + 1,blockTr[i][0] + 3,blockTr[i][1] + 1,0);   
      }
   }
}

void drawBlock(){
  for (int i = 0; i < 4; i++){
     vgaU.draw_line(blockTr[i][0],blockTr[i][1],blockTr[i][0] + 3,blockTr[i][1], color);
     vgaU.draw_line(blockTr[i][0],blockTr[i][1] + 1,blockTr[i][0] + 3,blockTr[i][1] + 1, color);   
  }
  for (int i = 0; i < 4; i++){
     blockTmp[0][0] = blockTr[0][0];
     blockTmp[0][1] = blockTr[0][1];
     blockTmp[1][0] = blockTr[1][0];
     blockTmp[1][1] = blockTr[1][1];
     blockTmp[2][0] = blockTr[2][0];
     blockTmp[2][1] = blockTr[2][1];
     blockTmp[3][0] = blockTr[3][0];
     blockTmp[3][1] = blockTr[3][1];
  }
}

void drawBlockTmp(){
  for (int i = 0; i < 4; i++){
     vgaU.draw_line(blockTmp[i][0],blockTmp[i][1],blockTmp[i][0] + 3,blockTmp[i][1], color);
     vgaU.draw_line(blockTmp[i][0],blockTmp[i][1] + 1,blockTmp[i][0] + 3,blockTmp[i][1] + 1, color);   
  }
}

//--------------------- This is the end of the main loop ----------------------------------------
//-----------------------------------------------------------------------------------------------